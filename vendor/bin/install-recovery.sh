#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:33554432:608fc905861fab31098f593c096bdd3c1730b9d0; then
  applypatch \
          --flash /vendor/etc/recovery.img \
          --target EMMC:/dev/block/by-name/recovery:33554432:608fc905861fab31098f593c096bdd3c1730b9d0 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
